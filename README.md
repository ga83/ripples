# Ripples Live Wallpaper for Android

This wallpaper responds to touch and renders waves that interfere with each other. It is open-source, licenced under GPL so anyone can contribute to it.

[https://play.google.com/store/apps/details?id=com.beachcafesoftware.rubywaves](https://play.google.com/store/apps/details?id=com.beachcafesoftware.rubywaves)