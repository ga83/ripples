#ifdef GL_ES
    precision highp float;
#endif

/*
** Guy Aaltonen
*/


varying lowp float widthf;
varying lowp float heightf;

float height(vec2 centre, float phase, float amplitude);

uniform float red1;
uniform float green1;
uniform float blue1;

uniform float red2;
uniform float green2;
uniform float blue2;

uniform float phase0;
uniform float phase1;
uniform float phase2;
uniform float phase3;
uniform float phase4;
uniform float phase5;
uniform float phase6;

 uniform float posx;
 uniform float posy;
 uniform float posx1;
 uniform float posy1;
 uniform float posx2;
 uniform float posy2;
 uniform float posx3;
 uniform float posy3;
 uniform float posx4;
 uniform float posy4;
 uniform float posx5;
 uniform float posy5;
 uniform float posx6;
 uniform float posy6;

uniform float amp0;
uniform float amp1;
uniform float amp2;
uniform float amp3;
uniform float amp4;
uniform float amp5;
uniform float amp6;

uniform int cyclePosition;

uniform int numPoints;

uniform float longside;
uniform float invwidthf;

uniform float landscape_aspect_ratio;
varying vec2 v_texCoords;

uniform float mix_strength;
varying vec4 v_color;



float PHI = 1.61803398874989484820459 * 00000.1; // Golden Ratio
float PI  = 3.14159265358979323846264 * 00000.1; // PI
float SQ2 = 1.41421356237309504880169 * 10000.0; // Square Root of Two

float gold_noise(vec2 coordinate, float seed);



void main()
{
 	highp float lum = 0.5;
  	vec2 vecta;
  	float totaladd = 0.0;

    vecta = vec2(posx , posy );   totaladd = totaladd + height(vecta, phase0, amp0) * float(numPoints > 0);
	vecta = vec2(posx1, posy1);   totaladd = totaladd + height(vecta, phase1, amp1) * float(numPoints > 1);
 	vecta = vec2(posx2, posy2);   totaladd = totaladd + height(vecta, phase2, amp2) * float(numPoints > 2);
	vecta = vec2(posx3, posy3);   totaladd = totaladd + height(vecta, phase3, amp3) * float(numPoints > 3);
	vecta = vec2(posx4, posy4);   totaladd = totaladd + height(vecta, phase4, amp4) * float(numPoints > 4);
	vecta = vec2(posx5, posy5);   totaladd = totaladd + height(vecta, phase5, amp5) * float(numPoints > 5);
	vecta = vec2(posx6, posy6);   totaladd = totaladd + height(vecta, phase6, amp6) * float(numPoints > 6);

	lum = lum + totaladd ;
 	clamp(lum, 0.0, 1.0);

    float r; float g; float b;

    float display = float(gold_noise(v_texCoords,lum) < abs(lum - 0.5));

    r = red1 * float(lum > 0.5) * display      +       red2 * float(lum < 0.5) * display;
    g = green1 * float(lum > 0.5) * display    +       green2 * float(lum < 0.5) * display;
    b = blue1 * float(lum > 0.5) * display     +       blue2 * float(lum < 0.5) * display;

    gl_FragColor = mix(vec4(r, g, b, 1.0) , v_color, mix_strength);
}




float height(vec2 position, float phase, float amplitude)
{
	float dx = (position[0] - v_texCoords[0]);
 	float dy = (position[1] - v_texCoords[1]) * landscape_aspect_ratio;

    float distancesq = dx * dx + dy * dy;
    float distance = sqrt(distancesq);

    return sin((distance * 60.0) + phase) * 0.25 * (1.0 / distance) * 0.5 * amplitude;
}



float gold_noise(vec2 coordinate, float seed){
    return fract(sin(dot(coordinate * (seed + PHI), vec2(PHI, PI))) * SQ2);
}


