#ifdef GL_ES
    precision highp float;
#endif


attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;

uniform mat4 u_projTrans;

varying vec4 v_color;
varying vec2 v_texCoords;


attribute float widthv;
attribute float heightv;
varying float widthf;
varying float heightf;

attribute float numpointsv;
varying float numpointsf;


uniform float positionsv[200];
//varying float positionsf[200];


attribute vec2 positionv1; // v for vertex shader
varying vec2 positionf1;   // f for fragment shader


 void main() {
    v_color = a_color;

 	widthf = widthv;
	heightf = heightv;
 	positionf1 = positionv1;

    v_texCoords = a_texCoord0;
    gl_Position = u_projTrans * a_position;

 }

