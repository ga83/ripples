		package com.beachcafesoftware.rubywaves;

import android.content.SharedPreferences;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;

public class RubyWavesLiveWallpaper extends AndroidLiveWallpaperService
{
	private SharedPreferences mPrefs;

	@Override
	public void onCreateApplication () { 
		super.onCreateApplication();

		final AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
//		         config.useGL20 = false;
		config.useCompass = false;
		config.useWakelock = false;
		config.useAccelerometer = false;
		config.getTouchEventsForLiveWallpaper = true;

        final ApplicationListener listener = new WallpaperListener();
 
		initialize(listener, config);
	}

	public static class WallpaperListener extends RipplesWallpaper implements AndroidWallpaperListener {
 		@Override
		public void create() {
 			super.create();
		};

 		@Override
		public void offsetChange (float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
			 
		}

		@Override
		public void previewStateChange (boolean isPreview) {
			
		}
	}
}
