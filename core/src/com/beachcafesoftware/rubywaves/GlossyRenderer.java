package com.beachcafesoftware.rubywaves;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import java.util.ArrayList;


/**
 * Created by ubuntu on 31/03/18.
 */

public class GlossyRenderer implements RipplesRenderer {

    ShaderProgram shader;
    ShaderProgram[] shaders;
    FrameBuffer frameBuffer;
    SpriteBatch sb;
    int currentFrame;
    private ArrayList<PointRipples> pointsRipples;  // pass in
    private float[] points = new float[4 * 5];
    private Texture t1;
    boolean useGradient;


    private float getSumAmplitude() {
        float total = 0;
            for (int i = 0; i < pointsRipples.size(); i++) {
                total += pointsRipples.get(i).amplitude * 0.1;
            }
            return Math.min(total,0.99f);
    }

    @Override
    public String createShader(int numPoints) {
        String shader = "";

        shader += getHeader();
        shader += getMain(numPoints);
        shader += getHeightFunction();

        System.out.println(shader);

        return shader;
    }


    private String getMain(int numPoints) {
        String main =
                "void main() {\n" +
                        "highp float lum = 0.5;\n" +
                        "lowp float totaladd = 0.0;\n";

        for(int i = 0; i < numPoints; i++) {
            main += "totaladd = totaladd + height(vec2(posx" + (i > 0 ? i : "") +  " , posy"  + (i > 0 ? i : "") + "), phase" + i + ", amp" + i + "  ) * float(numPoints > " + i + ");\n";
        }

        main +=
                "lum = lum + totaladd;\n" +
                "clamp(lum, 0.0, 1.0);\n" ;

                        main += "    float r; float g; float b;\n" +
                        "\n" +
                        "    float red1a = red1 + 0.25;\n" +
                        "    float green1a = green1 + 0.25;\n" +
                        "    float blue1a = blue1 + 0.25;\n" +
                        "    float red2a = red2 + 0.25;\n" +
                        "    float green2a = green2 + 0.25;\n" +
                        "    float blue2a = blue2 + 0.25;\n" +
                        "\n" +
                        "    r = abs(lum - 0.5) * red1a * float(lum > 0.5)       +       abs(lum - 0.5) * red2a * float(lum < 0.5);\n" +
                        "    g = abs(lum - 0.5) * green1a * float(lum > 0.5)     +       abs(lum - 0.5) * green2a * float(lum < 0.5);\n" +
                        "    b = abs(lum - 0.5) * blue1a * float(lum > 0.5)      +       abs(lum - 0.5) * blue2a * float(lum < 0.5);\n" +
                        "\n" +
                        "    gl_FragColor = mix(vec4(r, g, b, 1.0) , v_color, mix_strength);\n" +
                                "}";
        return main;
    }


    private String getHeader() {
        String header;
        String filepath = "glossy_header.txt";
        FileHandle handle = Gdx.files.internal(filepath);
        header = handle.readString();
        return header;
    }

    private String getHeightFunction() {
        String heightFunction;

        heightFunction = "float height(vec2 position, float phase, float amplitude)\n" +
                "{\n" +
                "    float dx = (position[0] - v_texCoords[0]);\n" +
                "    float dy = (position[1] - v_texCoords[1]) * landscape_aspect_ratio;\n" +
                "\n" +
                "    float distancesq = dx * dx + dy * dy;\n" +
                "    float distance = sqrt(distancesq);\n" +
                "\n" +
                "    return sin((distance * 60.0) + phase) * 0.25 * (1.0 / distance) * 0.5 * amplitude;\n" +
                "}\n";

        return heightFunction;
    }



    @Override
    public void draw(int width, int height, int numPoints, Color color1, Color color2) {

        if(currentFrame % 120 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
        }

        frameBuffer.begin();
        sb.begin();
        shader = shaders[numPoints];
        sb.setShader(shader);

        //System.out.println("np: " + numPoints);

        // mix strength
        shader.setUniformf("mix_strength", useGradient == true ? getSumAmplitude() : 0);

        // colors
        shader.setUniformf("red1",  color1.r);
        shader.setUniformf("green1",color1.g);
        shader.setUniformf("blue1", color1.b);

        shader.setUniformf("red2",  color2.r);
        shader.setUniformf("green2",color2.g);
        shader.setUniformf("blue2", color2.b);

        // height and width
        shader.setAttributef("heightv", (float) height, 0f, 0f, 0f);
        shader.setAttributef("widthv", (float) width, 0f, 0f, 0f);

        // spouts phase
        shader.setUniformf("phase0", pointsRipples.size() > 0 ? pointsRipples.get(0).phase : 0);
        shader.setUniformf("phase1", pointsRipples.size() > 1 ? pointsRipples.get(1).phase : 0);
        shader.setUniformf("phase2", pointsRipples.size() > 2 ? pointsRipples.get(2).phase : 0);
        shader.setUniformf("phase3", pointsRipples.size() > 3 ? pointsRipples.get(3).phase : 0);
        shader.setUniformf("phase4", pointsRipples.size() > 4 ? pointsRipples.get(4).phase : 0);
        shader.setUniformf("phase5", pointsRipples.size() > 5 ? pointsRipples.get(5).phase : 0);
        shader.setUniformf("phase6", pointsRipples.size() > 6 ? pointsRipples.get(6).phase : 0);

        // spouts amplitude
        shader.setUniformf("amp0", pointsRipples.size() > 0 ? pointsRipples.get(0).amplitude : 0);
        shader.setUniformf("amp1", pointsRipples.size() > 1 ? pointsRipples.get(1).amplitude : 0);
        shader.setUniformf("amp2", pointsRipples.size() > 2 ? pointsRipples.get(2).amplitude : 0);
        shader.setUniformf("amp3", pointsRipples.size() > 3 ? pointsRipples.get(3).amplitude : 0);
        shader.setUniformf("amp4", pointsRipples.size() > 4 ? pointsRipples.get(4).amplitude : 0);
        shader.setUniformf("amp5", pointsRipples.size() > 5 ? pointsRipples.get(5).amplitude : 0);
        shader.setUniformf("amp6", pointsRipples.size() > 6 ? pointsRipples.get(6).amplitude : 0);

        // spouts coordinates
        shader.setUniformf("posx",  pointsRipples.size() > 0 ? pointsRipples.get(0).x : -100);
        shader.setUniformf("posy",  pointsRipples.size() > 0 ? pointsRipples.get(0).y : -100);
        shader.setUniformf("posx1", pointsRipples.size() > 1 ? pointsRipples.get(1).x : -100);
        shader.setUniformf("posy1", pointsRipples.size() > 1 ? pointsRipples.get(1).y : -100);
        shader.setUniformf("posx2", pointsRipples.size() > 2 ? pointsRipples.get(2).x : -100);
        shader.setUniformf("posy2", pointsRipples.size() > 2 ? pointsRipples.get(2).y : -100);
        shader.setUniformf("posx3", pointsRipples.size() > 3 ? pointsRipples.get(3).x : -100);
        shader.setUniformf("posy3", pointsRipples.size() > 3 ? pointsRipples.get(3).y : -100);
        shader.setUniformf("posx4", pointsRipples.size() > 4 ? pointsRipples.get(4).x : -100);
        shader.setUniformf("posy4", pointsRipples.size() > 4 ? pointsRipples.get(4).y : -100);
        shader.setUniformf("posx5", pointsRipples.size() > 5 ? pointsRipples.get(5).x : -100);
        shader.setUniformf("posy5", pointsRipples.size() > 5 ? pointsRipples.get(5).y : -100);
        shader.setUniformf("posx6", pointsRipples.size() > 6 ? pointsRipples.get(6).x : -100);
        shader.setUniformf("posy6", pointsRipples.size() > 6 ? pointsRipples.get(6).y : -100);

        // number of points
        shader.setUniformi("numPoints",numPoints);

        // long side
        shader.setUniformf("longside",Math.max(width,height));
        shader.setUniformf("invwidthf",1.0f / width);
        shader.setUniformf("landscape_aspect_ratio", (float)height / width);

        float c1;
        float c2;
        float c3;
        float c4;

            c1 = color1.toFloatBits();
            c2 = color1.toFloatBits();
            c3 = color2.toFloatBits();
            c4 = color2.toFloatBits();

        points[0] = 0;
        points[1] = 0;
        points[2] = c1;
        points[3] = 0;
        points[4] = 0;
        points[5] = width;
        points[6] = 0;
        points[7] = c2;
        points[8] = 1;
        points[9] = 0;
        points[10] = width;
        points[11] = height;
        points[12] = c3;
        points[13] = 1;
        points[14] = 1;
        points[15] = 0;
        points[16] = height;
        points[17] = c4;
        points[18] = 0;
        points[19] = 1;

        sb.draw(t1, points, 0, 20);
        sb.end();

        frameBuffer.end();

        sb.begin();
        sb.setShader(null);

        sb.setColor(1,1,1,1.0f);
        sb.draw(frameBuffer.getColorBufferTexture(), 0f, 0f, width, height);
        sb.end();
    }


    @Override
    public String getShaderStatus() {
//        return "iscompiled: " + shader.isCompiled() + " : " + shader.getLog();
        return "";
    }

    public GlossyRenderer(ArrayList<PointRipples> pointsRipples, String imageQuality, String filtering, int width, int height, boolean useGradient, int maxPoints) {
        this.useGradient = useGradient;
        this.pointsRipples = pointsRipples;

        sb = new SpriteBatch();

        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGB565);
        pixmap.setColor(0,1,0,1);
        pixmap.fill();

        t1 = new Texture(pixmap);
        t1.draw(pixmap, 0, 0);
        pixmap.dispose();

        String vertexShader = Gdx.files.internal("vertex.glsl").readString();
        String fragmentShader = Gdx.files.internal("fragmentglossy.glsl").readString();
        shader = new ShaderProgram(vertexShader,fragmentShader);
        shader.pedantic = false;

        shaders = new ShaderProgram[maxPoints + 1];

        for(int i = 0; i <= maxPoints; i++) {
            shaders[i] = new ShaderProgram(vertexShader, createShader(i));
            System.out.println( "iscompiled: " + shaders[i].isCompiled() + " : " + shaders[i].getLog());
        }


        if(imageQuality.equals("high")) {
            frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width / 1, height / 1, false);
        }
        else if(imageQuality.equals("medium")){
            frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width / 2, height / 2, false);
        }
        else if(imageQuality.equals("low")){
            frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width / 3, height / 3, false);
        }
        else if(imageQuality.equals("verylow")){
            frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width / 6, height / 6, false);
        }

        Texture.TextureFilter filter = filtering.equals("nearest") ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
        frameBuffer.getColorBufferTexture().setFilter(filter,filter);


    }

}
