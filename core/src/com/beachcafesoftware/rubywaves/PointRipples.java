package com.beachcafesoftware.rubywaves;

import java.util.Random;

/*
** Guy Aaltonen
*/

public class PointRipples {

    static float D_AMPLITUDE = 0.01f;

    float x;
    float y;
    float dp;
    float phase;
    float amplitude;
    boolean increasing;


     public void update() {
        phase -= dp;

        if(increasing == false) {
            amplitude -= D_AMPLITUDE;
        }
        else {
            amplitude += D_AMPLITUDE;
            if(amplitude > 1.0) {
                increasing = false;
            }
        }

        if(phase > 2.0 * Math.PI) { phase -= 2.0 * Math.PI; }
        if(phase < -2.0 * Math.PI) { phase += 2.0 * Math.PI; }
    }

    public PointRipples(float x, float y,float dp, float amplitude) {
        this.x = x;
        this.y = y;
        this.dp = dp;
        this.amplitude = amplitude;
        this.increasing = true;
    }
}