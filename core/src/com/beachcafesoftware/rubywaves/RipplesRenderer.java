package com.beachcafesoftware.rubywaves;


import com.badlogic.gdx.graphics.Color;

interface RipplesRenderer {

    public String createShader(int numPoints);
    public void draw(int width, int height, int numPoints,Color color1, Color color2);
    public String getShaderStatus();
}
