package com.beachcafesoftware.rubywaves;

import java.util.ArrayList;
import java.util.Random;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.TimeUtils;

/*
** Guy Aaltonen
*/


// TODO: do not release this again until shader is optimised: ie one shader for every possible number of active points.
public class RipplesWallpaper extends ApplicationAdapter implements InputProcessor
{
	public static boolean prefsChanged;

	private final int MAX_POINTS = 7;


	private int fps = 30;
	private int currentFrame = 0;
	private int maxFrame = 10 * fps;

	private Random rnd;

	private ArrayList<PointRipples> pointsRipples = new ArrayList<PointRipples>();
	private Preferences prefs;
 	private FPSLogger fps1 = new FPSLogger();

	private long runningTime;
	private long lastSwitch;
 	private int numPoints;
	private int framesUntilNextTouchAllowed = 0;

	private static int FRAMES_BETWEEN_TOUCHES = 4;
	private RipplesRenderer ripplesRenderer;

	private Color color1;
	private Color color2;

	private String imageQuality;
	private String rendererName;
	private String filtering;
	private boolean useGradient;


	@Override
	public void create () {
 		rnd = new Random();
		Gdx.input.setInputProcessor(this);
 		randomise();
 	}

	public void randomise() {
		prefs =  Gdx.app.getPreferences("cube2settings");
		loadPreferences();
		ripplesRenderer = getRenderer();
		System.out.println(ripplesRenderer.getShaderStatus());
		this.lastSwitch = TimeUtils.millis();
	}

	private void loadPreferences() {
 		String numPointsS = prefs.getString("numpoints");
		String runningTimeS = prefs.getString("runningtime");
		String rendererS = prefs.getString("renderer");
		String color1S = prefs.getString("color1");
		String color2S = prefs.getString("color2");
		String imageQualityS = prefs.getString("imagequality");
		String filteringS = prefs.getString("filtering");
		String gradientS = prefs.getString("gradient");

		// defaults if empty
		if(numPointsS.equals(""))			{ prefs.putString("numpoints","5"); }
		if(runningTimeS.equals(""))			{ prefs.putString("runningtime","20"); }
		if(rendererS.equals("")) 			{ prefs.putString("renderer","glossy"); }
		if(color1S.equals(""))				{ prefs.putString("color1","#FF0000FF"); }
		if(color2S.equals(""))				{ prefs.putString("color2","#0000FFFF"); }
		if(imageQualityS.equals(""))		{ prefs.putString("imagequality","medium"); }
		if(filteringS.equals("")) 			{ prefs.putString("filtering","bilinear"); }
		if(gradientS.equals("")) 			{ prefs.putString("gradient","false"); }

		prefs.flush();

		numPoints = 7;
		runningTime = Integer.valueOf(prefs.getString("runningtime"));
		color1 = Color.valueOf(prefs.getString("color1"));
		color2 = Color.valueOf(prefs.getString("color2"));
		imageQuality = prefs.getString("imagequality");
		rendererName = prefs.getString("renderer");
		useGradient = Boolean.valueOf(prefs.getString("gradient"));
		filtering = filteringS;
 	}

	private RipplesRenderer getRenderer() {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		System.out.println("-" + rendererName + "-");
		if(rendererName.equals("glossy")) { return new GlossyRenderer(pointsRipples,imageQuality,filtering, width, height,useGradient,MAX_POINTS); }
  		if(rendererName.equals("spotty")) { return new SpottyRenderer(pointsRipples,imageQuality,filtering, width, height,useGradient,MAX_POINTS); }
   		if(rendererName.equals("spotty2")) { return new Spotty2Renderer(pointsRipples,imageQuality,filtering, width, height,useGradient,MAX_POINTS); }
		return null;
	}



	private void removeDeadPoints() {
		for(int i = 0; i < pointsRipples.size(); i++) {
			if(pointsRipples.get(i).amplitude < -0.0001) {
				pointsRipples.remove(i);
			}
		}
	}

	@Override
	public void render ()
	{
 		if(RipplesWallpaper.prefsChanged == true)
		{
			currentFrame = maxFrame;
			this.lastSwitch = 0;
			prefsChanged = false;
			randomise();
		}

		framesUntilNextTouchAllowed --;

 		removeDeadPoints();

 		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
 		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		for (int i = 0; i < pointsRipples.size(); i++) {
			pointsRipples.get(i).update();
  		}

		// RENDER
 		ripplesRenderer.draw(width,height,pointsRipples.size(),color1,color2);

		currentFrame++;
 		fps1.log();
 	}


	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}



	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		if(framesUntilNextTouchAllowed <= 0 && pointsRipples.size() < MAX_POINTS) {
			pointsRipples.add(new PointRipples((float) screenX / width, (float) screenY / height, 0.12f, 0));
			framesUntilNextTouchAllowed = FRAMES_BETWEEN_TOUCHES;
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
//		touchDown(screenX,screenY,pointer,0);
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}





